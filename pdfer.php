<?php

$boosh="123-456-TEST-789-J";
$skadoosh="12/12/2013";
require_once('fpdf.php');
require_once('fpdi.php');

$pdf = new FPDI();
$pdf->AddPage();

//set the source file
$pagecount = $pdf->setSourceFile("Petz_HealthCard_v14.pdf");

//import the first page of the file
$tpl = $pdf->importPage(1);
$pdf->useTemplate($tpl);

//information for top card
$pdf->SetXY(15,-126);
$pdf->SetFont('Arial', 'I', 8);
$pdf->Cell(50, 10, "$boosh,", 0, 0, "C");

$pdf->SetXY(25, -122);
$pdf->Cell(50, 10, "$skadoosh,", 0, 0, "C");

//information for bottom card
$pdf->SetXY(15, -73);
$pdf->Cell(50, 10, "$boosh,", 0, 0, "C");

$pdf->SetXY(25, -69);
$pdf->Cell(50, 10, "$skadoosh,", 0, 0, "C");

$pdf->Output();
?>