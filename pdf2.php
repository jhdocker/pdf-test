<?php

require_once('fpdf.php');
require_once('fpdi.php');

$f_name='Jackson';
$l_name='Dockery';
$address="1018 Twyckenham dr. Greensboro, NC 27408";

$pdf = new FPDI();
$pdf->AddPage();

$pagecount = $pdf->setSourceFile('adr.pdf');

$tpl = $pdf->importPage(1);
$pdf->useTemplate($tpl);

$pdf->SetFont('Arial', 'I', 8);

$pdf->SetXY(52, 74);
$pdf->Cell(50, 10, $_POST['fname'] . " " . $_POST['lname'], 0, 1, "L");

$pdf->SetXY(52, 79);
$pdf->Cell(50, 10, $_POST['saddress'], 0, 1, "L");

$pdf->SetXY(52, 84);
$pdf->Cell(50, 10, $_POST['address'], 0, 1, "L");

$pdf->SetXY(55, 62);
$pdf->Cell(50, 10, $_POST['acct'], 0, 1, "L");

$pdf->Output();

?>